# TalkingPumpkin

Simple arduino project that combines IR remote, a distance sensor and adafruit 
sound board.

## Way it works

Load ogg sound files into adafruit sound board (needs to be disconnected from 
arduino) currently arduino code supports 10 sounds to be activated by remote. 

8th file will be heard if something stands 40 cm from the ultra sonic sensor. 

Stop sound with the stop button on remote. 

## Wire diagram

![See attached image for wire diagram.](wireDiagram.png)


## Bill of materials

* elegoo.com uno R3 starter kit: prototype expansion board and, remote, 
IR reciever module, and uno r3 controller board
* adafruit sound board, 9 V battery and connector, usb printer
* Cables
* cheap speakers with audio jack. 

## Tutorials 

to develop this I have merged different projects:

* [Sound board](https://github.com/adafruit/Adafruit_Soundboard_library)
* [IR remote library]( https://github.com/z3t0/Arduino-IRremote)
* [ultra sonic sensor](https://github.com/gamgine/HCSR04-ultrasonic-sensor-lib)
* [elegoo tutorial](https://www.elegoo.com/tutorial/Elegoo%20Super%20Starter%20Kit%20for%20UNO%20V1.0.2018.10.25.zip)


## Trouble shooting

Make sure your audio file starts from the begining. if there is silence at the 
start you will feel impatient with remote or sensor, thinking it is not working. 

Delays can be changed I found that the last delay in the loop is a critical one 
but I don't know how long it will be. 

Delays also mean that pressing button on remote in a row might trigger "repeat" 
or even a 55 when you meant a 5. 

Please help improve the work by raising issues or doing pull requests. 