/* 
  Menu driven control of a sound board over UART.
  Commands for playing by # or by name (full 11-char name)
  Hard reset and List files (when not playing audio)
  Vol + and - (only when not playing audio)
  Pause, unpause, quit playing (when playing audio)
  Current play time, and bytes remaining & total bytes (when playing audio)

  Connect UG to ground to have the sound board boot into UART mode
*/

//www.elegoo.com
//2016.12.9

#include "IRremote.h"

#include <SoftwareSerial.h>
#include "Adafruit_Soundboard.h"
#include <HCSR04.h>

// Choose any two pins that can be used with SoftwareSerial to RX & TX
#define SFX_TX 5
#define SFX_RX 6

// Connect to the RST pin on the Sound Board
#define SFX_RST 4

// You can also monitor the ACT pin for when audio is playing!

// we'll be using software serial
SoftwareSerial ss = SoftwareSerial(SFX_TX, SFX_RX);

// pass the software serial to Adafruit_soundboard, the second
// argument is the debug port (not used really) and the third 
// arg is the reset pin
Adafruit_Soundboard sfx = Adafruit_Soundboard(&ss, NULL, SFX_RST);
// can also try hardware serial with
// Adafruit_Soundboard sfx = Adafruit_Soundboard(&Serial1, NULL, SFX_RST);

int receiver = 11; // Signal Pin of IR receiver to Arduino Digital Pin 11

/*-----( Declare objects )-----*/
IRrecv irrecv(receiver);     // create instance of 'irrecv'
decode_results results;      // create instance of 'decode_results'

HCSR04 hc(10,9);//initialisation class HCSR04 (trig pin , echo pin)



/*-----( Function )-----*/
void translateIR() // takes action based on IR code received

// describing Remote IR codes 

{

  switch(results.value)

  {

  case 0xFF6897: 
      Serial.println("0");
      /*uint8_t n = 0;
      */
           if (! sfx.playTrack((uint8_t)0)) {
          Serial.println("Failed to play track?");
          }
          Serial.println(results.value);
      break;
  
  case 0xFF30CF: 
      Serial.println("1");

           if (! sfx.playTrack((uint8_t)1)) {
          Serial.println("Failed to play track?");
          }
          Serial.println(results.value);
      break;

  case 0xFF18E7: Serial.println("2");
           if (! sfx.playTrack((uint8_t)2)) {
          Serial.println("Failed to play track?");
          }
      break;
  
  case 0xFF7A85: Serial.println("3");   
           if (! sfx.playTrack((uint8_t)3)) {
          Serial.println("Failed to play track?");
          }
      break;
  
  case 0xFF10EF: Serial.println("4");    
           if (! sfx.playTrack((uint8_t)4)) {
          Serial.println("Failed to play track?");
          }
      break;

  case 0xFF38C7: Serial.println("5");      
           if (! sfx.playTrack((uint8_t)5)) {
          Serial.println("Failed to play track?");
          }
      break;
      
  case 0xFF5AA5: Serial.println("6");
           if (! sfx.playTrack((uint8_t)6)) {
          Serial.println("Failed to play track?");
          }
      break;

    case 0xFF42BD: Serial.println("7");    
                       if (! sfx.playTrack((uint8_t)7)) {
          Serial.println("Failed to play track?");
          }
          break;
  case 0xFF4AB5: Serial.println("8");    
                     if (! sfx.playTrack((uint8_t)8)) {
          Serial.println("Failed to play track?");
          }
          break;
          
  case 0xFF52AD: Serial.println("9");    
                     if (! sfx.playTrack((uint8_t)9)) {
          Serial.println("Failed to play track?");
          }
          
          break;

  case 0xFFFFFFFF: 
        Serial.println(" REPEAT");
        Serial.println(results.value);
        break;  
  case 0xFFE21D: Serial.println("FUNC/STOP"); 
           if (! sfx.stop() ) Serial.println("Failed to stop"); 
           break;
  default: 
    Serial.println("IR other button   ");
    Serial.println(results.value);

  }// End Case

  delay(500); // Do not get immediate repeat


} //END translateIR






void setup() {
  Serial.begin(115200);
  Serial.println("Adafruit Sound Board!");
  // softwareserial at 9600 baud
  ss.begin(9600);
  // can also do Serial1.begin(9600)

  if (!sfx.reset()) {
    Serial.println("Not found");
    while (1);
  }
  Serial.println("SFX board found");
  Serial.println("IR Receiver Button Decode"); 
  irrecv.enableIRIn(); // Start the receiver
}


void loop() {

  
   flushInput();


    
    if  ( hc.dist()< 40)
      {
      
          // if (! sfx.stop() ) Serial.println("Failed to stop"); 
           if (! sfx.playTrack((uint8_t)7)) {
          Serial.println("Failed to play track?");
           }
            
      }  

  
        if (  irrecv.decode(&results)) // have we received an IR signal?
         {    
          translateIR();
          irrecv.resume(); // receive the next value
          Serial.println("IR online after IF");
          }
          
 delay(100); //OMG this delay made me loose half a day! critical to make it work. 
}
 



/************************ MENU HELPERS ***************************/

void flushInput() {
  // Read all available serial input to flush pending data.
  uint16_t timeoutloop = 0;
  while (timeoutloop++ < 40) {
    while(ss.available()) {
      ss.read();
      timeoutloop = 0;  // If char was received reset the timer
    }
    delay(1);
  }
}

char readBlocking() {
  while (!Serial.available());
  return Serial.read();
}

uint16_t readnumber() {
  uint16_t x = 0;
  char c;
  while (! isdigit(c = readBlocking())) {
    //Serial.print(c);
  }
  Serial.print(c);
  x = c - '0';
  while (isdigit(c = readBlocking())) {
    Serial.print(c);
    x *= 10;
    x += c - '0';
  }
  return x;
}

uint8_t readline(char *buff, uint8_t maxbuff) {
  uint16_t buffidx = 0;
  
  while (true) {
    if (buffidx > maxbuff) {
      break;
    }

    if (Serial.available()) {
      char c =  Serial.read();
      //Serial.print(c, HEX); Serial.print("#"); Serial.println(c);

      if (c == '\r') continue;
      if (c == 0xA) {
        if (buffidx == 0) {  // the first 0x0A is ignored
          continue;
        }
        buff[buffidx] = 0;  // null term
        return buffidx;
      }
      buff[buffidx] = c;
      buffidx++;
    }
  }
  buff[buffidx] = 0;  // null term
  return buffidx;
}



/************************ MENU HELPERS ***************************/
